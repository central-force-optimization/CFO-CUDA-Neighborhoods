#include <iostream>
#include <iomanip>
#include <vector>
#include <math.h>
#include <cstdlib>
#include <cmath>
#include "hr_time.h"
#include <fstream>
#include <sstream>
#include <limits>
#include <omp.h>

using namespace std;

typedef vector<int> 		iArray1D;
typedef vector<long double> ldArray1D;
typedef vector<ldArray1D> 	ldArray2D;
typedef vector<ldArray2D> 	ldArray3D;

#define PI_F 3.141592654f 

__global__ void calculateA(float *R, float *A, float *M, int Np, int Nd, float Alpha, float Beta){
    
    int p     = blockIdx.x;  // Current Block
    int i     = threadIdx.x; // Current Thread
    int l  = (Np+p-1) % Np;
    int r = (p+1) % Np;
    float SumSQ, Numerator, Denom, localA;  
       
    //A[p*Nd+i] = 0; // Move to local memory ??
    localA = 0;    

    // Left Neighbor
	SumSQ = 0.0;
	for (int L = 0; L < Nd; L++) {
    	SumSQ = SumSQ + pow(R[l*Nd+L] - R[p*Nd+L], 2);
	}

	if(SumSQ != 0){
		Denom = sqrt(SumSQ);
		Numerator = 0.0;
		if(M[l] - M[p] >= 0.0){
		    Numerator = (M[l]- M[p]);
		}
		//A[p*Nd+i] = A[p*Nd+i] + (R[k*Nd+i] - R[p*Nd+i]) * pow(Numerator,Alpha)/(pow(Denom,Beta));
		localA = localA + (R[l*Nd+i] - R[p*Nd+i]) * pow(Numerator,Alpha)/(pow(Denom,Beta));
	}
	
	// Right Neighbor
	SumSQ = 0.0;
	for (int L = 0; L < Nd; L++) {
    	SumSQ = SumSQ + pow(R[r*Nd+L] - R[p*Nd+L], 2);
	}

	if(SumSQ != 0){
		Denom = sqrt(SumSQ);
		Numerator = 0.0;
		if(M[r] - M[p] >= 0.0){
		    Numerator = (M[r]- M[p]);
		}
		//A[p*Nd+i] = A[p*Nd+i] + (R[k*Nd+i] - R[p*Nd+i]) * pow(Numerator,Alpha)/(pow(Denom,Beta));
		localA = localA + (R[r*Nd+i] - R[p*Nd+i]) * pow(Numerator,Alpha)/(pow(Denom,Beta));
	}
		
	
	A[p*Nd+i] = localA;
}

long double sigmoid(long double x){
	return 1/(1+exp(-x));
}
long double F1(ldArray3D& R, int Nd, int p, int j) { // Sphere
	long double Z=0,  Xi;

	for(int i=0; i<Nd; i++){
		Xi = R[p][i][j];
		Z = Z + pow(Xi,2);
	}
	return -Z;
}

long double F2(ldArray3D& R, int Nd, int p, int j) { // Sphere
	long double Z=0,  Xi;

	for(int i=0; i<Nd; i++){
		Xi = R[p][i][j];
		Z += (pow(Xi,2) - 10 * cos(2*PI_F*Xi) + 10);
	}
	return -Z;
}

long double F3(ldArray3D& R, int Nd, int p, int j) { // Sphere
	long double Z, Sum, Prod, Xi;

    Z = 0; Sum = 0; Prod = 1;
    
	for(int i=0; i<Nd; i++){
		Xi = R[p][i][j];
		Sum  += Xi*Xi;
		Prod *= cos(Xi/sqrt((double)i)+1)/4000.0f; 
		
		if(isnan(Prod)) Prod = 1;
    }
	
	Z = Sum - Prod;
	
	return -Z;
}

long double F4(ldArray3D& R, int Nd, int p, int j) {
	long double Z=0, Xi, XiPlus1;

	for(int i=0; i<Nd-1; i++){
		Xi = R[p][i][j];
		XiPlus1 = R[p][i+1][j];
		
		Z = Z + (100*(XiPlus1-Xi*Xi)*(XiPlus1-Xi*Xi) + (Xi-1)*(Xi-1));
	}
	return -Z;
}


/*
Nd=30, X-Min=-10, X-Max=10
FMin = 0
*/
//long double F2(ldArray3D& R, int Nd, int p, int j) {
//	long double Z=0, Sum=0, Prod=1, Xi;

//	for(int i=0; i<Nd; i++){
//		Xi = R[p][i][j];
//		Sum = Sum + fabs(Xi);
//		Prod = Prod * fabs(Xi);
//	}

//	Z = Sum + Prod;

//	return -Z;
//}
///*

//Nd=30, X-Min=-100, X-Max=100
//FMin = 0
//*/
//long double F3(ldArray3D& R, int Nd, int p, int j) {
//	long double Z=0, Sum=0, Xk;

//	for(int i=0; i<Nd; i++){
//		Sum = 0;
//		for(int k=0; k<i; k++){
//			Xk = R[p][i][j];
//			Sum = Sum + Xk;
//		}

//		Z = Z + pow(Sum,2);
//	}
//	return -Z;
//}
///*

//Nd=30, X-Min=-100, X-Max=100
//FMin = 0
//*/
//long double F4(ldArray3D& R, int Nd, int p, int j) {
//	long double MaxXi=0, Xi;

//	MaxXi = -INFINITY;
//	for(int i=0; i<Nd; i++){
//		Xi = fabs(R[p][i][j]);
//		if(Xi >= MaxXi){
//			MaxXi = Xi;
//		}
//	}
//	return -MaxXi;
//}
///*

//Nd=30, X-Min=-30, X-Max=30
//FMin = 0
//*/
//long double F5(ldArray3D& R, int Nd, int p, int j) {
//	long double Z=0, Xi, XiPlus1;

//	for(int i=0; i<Nd-1; i++){
//		Xi = R[p][i][j];
//		XiPlus1 = R[p][i+1][j];
//		Z = Z + pow(100 * pow(XiPlus1-pow(Xi,2),2)+(Xi-1),2);
//	}
//	return -Z;
//}

bool hasFitnessSaturated(int nSteps, int j, int Np, int Nd,	ldArray2D& M, ldArray3D& R, int DiagLength) {

	long double fitnessSatTol = 0.000001;
	long double bestFitness = -INFINITY;
	long double bestFitnessStepJ = -INFINITY;

	if (j < nSteps + 10)
		return false;

	long double sumOfBestFitness = 0;

	for (int k = j - nSteps + 1; k <= j; k++) {
		bestFitness = -INFINITY;

		for (int p = 0; p < Np; p++) {

			if (M[p][k] >= bestFitness) {
				bestFitness = M[p][k];
			}
		}

		if(k == j){
			bestFitnessStepJ = bestFitness;
		}

		sumOfBestFitness += bestFitness;
	}

	if (fabs(sumOfBestFitness / nSteps - bestFitnessStepJ) <= fitnessSatTol) {
		return true;
	}

	return false;
}

long double UnitStep(long double X) {
	if (X < 0.0)
		return 0.0;

	return 1.0;
}
void GetBestFitness(ldArray2D& M, int Np, int stepNumber, long double& bestFitness, int& bestProbeNumber, int& bestTimeStep){

	bestFitness = M[0][0];

	for(int i=0; i<stepNumber; i++){
		for(int p=0; p<Np; p++){
			if(M[p][i] >= bestFitness){
				bestFitness = M[p][i];
				bestProbeNumber = p;
				bestTimeStep = i;
			}
		}
	}
}
int getMaxProbes(int i){

	if(i >= 1 && i <= 6){
		return 14;
	}else if(i >= 7 && i <= 10){
		return 12;
	}else if(i >= 11 && i <= 15){
		return 10;
	}if(i >= 16 && i <= 20){
		return 8;
	}if(i >= 21 && i <= 30){
		return 6;
	}

	return 4;

}
void MiniBenchmarkCFO(int& bestNp, int Nd, int& Nt, long double& bestGamma, long double oMin, long double oMax, long double(*objFunc)(ldArray3D& ,int, int, int), string functionName) {

	vector<long double> xMin;
	vector<long double> xMax;
	long double bestFitness = -INFINITY;
	long double bestFitnessThisRun = -INFINITY;
	long double bestFitnessOverall = -INFINITY;
	long double Gamma, Frep;

	long double deltaFrep = 0.1;
	long double DeltaXi;
	long double DiagLength = 0;
	long double Alpha = 1, Beta = 2;
	ldArray3D bestR;
	ldArray3D bestA;
	ldArray2D bestM;
	string s;
	stringstream ss;
	ofstream myFile;

	int lastStep, Np;
	int bestProbeNumber = 0, bestTimeStep = 0;
	int bestProbeNumberThisRun = 0, bestTimeStepThisRun = 0;
	int bestProbeNumberOverall = 0, bestTimeStepOverall = 0;
	int maxProbesPerDimension = getMaxProbes(Nd);
	long double NumGammas = 21;
	int numEvals, bestNumEvals;
	int bestNpNd;
	int	lastStepBestRun;

    CStopWatch timer, timer1, timer2;
    long double positionTime = 0, correctionTime = 0, fitnessTime = 0, accelTime = 0,
                shrinkTime = 0,   convergeTime = 0,   totalTime = 0,   copyTime = 0, tTime = 0;
    
	string fileName;

	xMin.resize(Nd, oMin);
	xMax.resize(Nd, oMax);

    timer.startTimer();
	for(int numProbesPerAxis = 4; numProbesPerAxis <= maxProbesPerDimension; numProbesPerAxis += 2){
		Np = numProbesPerAxis*Nd;

		ldArray3D R(Np, ldArray2D(Nd, ldArray1D(Nt, 0)));
		ldArray3D A(Np, ldArray2D(Nd, ldArray1D(Nt, 0)));
		ldArray2D M(Np, ldArray1D(Nt, 0));
		
		float *linearR = new float[Np*Nd], *linearA = new float[Np*Nd], *linearM = new float[Np],
              *rGpu    = new float[Np*Nd], *aGpu    = new float[Np*Nd], *mGpu    = new float[Np];

		for(int GammaNumber=1; GammaNumber <=NumGammas; GammaNumber++){
			Gamma = (GammaNumber-1)/(NumGammas-1);

			// Reset Matrices
			for(int j=0; j<Nt; j++){
				for (int p = 0; p < Np; p++) {
					for (int i = 0; i < Nd; i++) {
						R[p][i][j] = 0;	A[p][i][j] = 0;
					}
					M[p][j] = 0;
				}
			}

			// Reset Bests
			bestFitness 	= bestFitnessThisRun 		=  -INFINITY;
			bestProbeNumber = bestProbeNumberThisRun 	= 0;
			bestTimeStep 	= bestTimeStepThisRun 		= 0;
			
			positionTime = 0; correctionTime = 0; fitnessTime = 0;
			accelTime = 0; shrinkTime = 0; convergeTime = 0;
			
			numEvals = 0;
			// End Reset Values
            
            timer2.startTimer();
			for (int i = 0; i < Nd; i++) {
				DiagLength += pow(xMax[i] - xMin[i], 2);
			}
			DiagLength = sqrt(DiagLength);

			// Initial Probe Distribution
			for (int i = 0; i < Nd; i++) {
				for (int p = 0; p < Np; p++) {
					R[p][i][0] = xMin[i] + Gamma * (xMax[i] - xMin[i]);
				}
			}
			for (int i = 0; i < Nd; i++) {
				DeltaXi = (xMax[i] - xMin[i]) / (numProbesPerAxis-1);
				int p;

				for (int k = 0; k < numProbesPerAxis; k++) {
					p = k + numProbesPerAxis * i;
					R[p][i][0] = xMin[i] + k * DeltaXi;
				}
			}

			// Set Initial Acceleration to 0
			for (int p = 0; p < Np; p++) {
				for (int i = 0; i < Nd; i++) {
					A[p][i][0] = 0;
				}
			}
			// Compute Initial  Fitness
			for (int p = 0; p < Np; p++) {
				M[p][0] = objFunc(R, Nd, p, 0);
				numEvals++;
			}
			// Time Steps
			lastStep = Nt;
			bestFitnessThisRun = M[0][0];// -INFINITY;
			Frep = 0.5;

			for (int j = 1; j < Nt; j++) {
				// Compute new positions
				timer1.startTimer();
				for (int p = 0; p < Np; p++) {
					for (int i = 0; i < Nd; i++) {
						R[p][i][j] = R[p][i][j - 1] + A[p][i][j - 1];
					}
				}
				timer1.stopTimer();
				positionTime += timer1.getElapsedTime();
				
				// Correct any errors
				timer1.startTimer();
				for (int p = 0; p < Np; p++) {
					for (int i = 0; i < Nd; i++) {

						if (R[p][i][j] < xMin[i]) {
							R[p][i][j] = max(xMin[i] + Frep * (R[p][i][j - 1] - xMin[i]), xMin[i]);
						}
						if (R[p][i][j] > xMax[i]) {
							R[p][i][j] = min(xMax[i] - Frep * (xMax[i] - R[p][i][j - 1]), xMax[i]);
						}
					}
				}
				timer1.stopTimer();
				correctionTime += timer1.getElapsedTime();
				// Update Fitness
				timer1.startTimer();
				for (int p = 0; p < Np; p++) {
					M[p][j] = objFunc(R, Nd, p, j);
    				numEvals++;

					if(M[p][j] >= bestFitness){
						bestFitness 	= M[p][j];
						bestTimeStep 	= j;
						bestProbeNumber = p;
					}
				}

				if(bestFitness >= bestFitnessThisRun){
					bestFitnessThisRun 		= bestFitness;
					bestProbeNumberThisRun 	= bestProbeNumber;
					bestTimeStepThisRun 	= bestTimeStep;
				}
				timer1.stopTimer();
				fitnessTime += timer1.getElapsedTime();
				
				// Acceleration
		        // Data for GPU Computations
		        timer1.startTimer();
		        for(int p=0; p<Np; p++){
		            linearM[p] = M[p][j];
		            for(int i=0; i<Nd; i++){
		                linearR[p*Nd+i] = R[p][i][j];   
		                linearA[p*Nd+i] = A[p][i][j];   
		            }
		        }
                timer1.stopTimer();
                copyTime += timer1.getElapsedTime();
                
                timer1.startTimer();
                cudaMalloc((void**) &rGpu, Np*Nd*sizeof(float));  
                cudaMalloc((void**) &aGpu, Np*Nd*sizeof(float));
                cudaMalloc((void**) &mGpu, Np*sizeof(float));
            
                cudaMemcpy(rGpu, linearR, Np*Nd*sizeof(float), cudaMemcpyHostToDevice); 
                cudaMemcpy(aGpu, linearA, Np*Nd*sizeof(float), cudaMemcpyHostToDevice);
                cudaMemcpy(mGpu, linearM, Np*sizeof(float), cudaMemcpyHostToDevice);
		        calculateA<<<Np, Nd>>>(rGpu, aGpu, mGpu, Np, Nd, Alpha, Beta);
                cudaMemcpy(linearA, aGpu, Np*Nd*sizeof(float), cudaMemcpyDeviceToHost);
                
	            for(int p=0; p<Np; p++){
	                for(int i=0; i<Nd; i++){
	                    A[p][i][j] = linearA[p*Nd+i];   
	                }
		        }
            	cudaFree(rGpu);
	            cudaFree(aGpu);
            	cudaFree(mGpu);		
            	
				timer1.stopTimer();
				accelTime += timer1.getElapsedTime();
				// Adjust Frep
				Frep += deltaFrep;
				if(Frep > 1.0){
					 Frep = 0.05;
				}

				// Shrink Boundaries
				timer1.startTimer();
				if(j % 10 == 0 && j >= 20){
					for(int i=0; i<Nd; i++){
						xMin[i] = xMin[i] + (R[bestProbeNumber][i][bestTimeStep] - xMin[i])/2;
						xMax[i] = xMax[i] - (xMax[i] - R[bestProbeNumber][i][bestTimeStep])/2;
					}
					// Correct any errors

					for (int p = 0; p < Np; p++) {
						for (int i = 0; i < Nd; i++) {

							if (R[p][i][j] < xMin[i]) {
								R[p][i][j] = max(xMin[i] + Frep * (R[p][i][j - 1] - xMin[i]), xMin[i]);
							}
							if (R[p][i][j] > xMax[i]) {
								R[p][i][j] = min(xMax[i] - Frep * (xMax[i] - R[p][i][j - 1]), xMax[i]);
							}
						}
					}
				}
				timer1.stopTimer();
				shrinkTime += timer1.getElapsedTime();

				//
				// Test for fitness saturation
				//
				lastStep = j;
				timer1.startTimer();
				if (hasFitnessSaturated(25, j, Np, Nd, M, R, DiagLength)) {
				    timer1.stopTimer();
				    convergeTime += timer1.getElapsedTime();
					break;
				}
				timer1.stopTimer();
				convergeTime += timer1.getElapsedTime();

			}//End Time Steps

            timer2.stopTimer();
            tTime = timer2.getElapsedTime();
            
			if(bestFitnessThisRun >= bestFitnessOverall){
				bestFitnessOverall 		= bestFitnessThisRun;
				bestProbeNumberOverall 	= bestProbeNumberThisRun;
				bestTimeStepOverall 	= bestTimeStepThisRun;

				bestNpNd 		= numProbesPerAxis;
				bestGamma 		= Gamma;
				lastStepBestRun = lastStep;
				bestNp			= Np;
				bestNumEvals    = numEvals;
			}

			//
			//Reset Decision Space
			//
			xMin.clear(); xMax.clear();
			xMin.resize(Nd, oMin);
			xMax.resize(Nd, oMax);
                    
//			cout << "Function Name:        		" << functionName << endl;
//			cout << "Best Fitness:    			" << setprecision(18) << bestFitness << endl;
//			cout << "Best Probe #:    			" << setprecision(0) << bestProbeNumber << endl;
//			cout << "Best Time Step:  			" << bestTimeStep << endl;
//			cout << "Gamma:           			" << setprecision(18) <<  Gamma << endl;
//			cout << "Probes Per Axis: 			" << numProbesPerAxis << endl;
//			cout << "Number of Probes:			" << Np << endl;
//			cout << "Last Step:       			" << setprecision(0) << lastStep << endl << endl;
            cout    << functionName << ","
                    << bestFitness  << "," 
                    << bestProbeNumber << ","
                    << bestTimeStep << ","
                    << Gamma << ","
                    << numProbesPerAxis << ","
                    << Np << ","
                    << Nd << ","
                    << lastStep << ","
                    << numEvals << ","
                    << positionTime << ","
                    << correctionTime << ","
                    << fitnessTime << ","
                    << accelTime << ","
                    << shrinkTime << ","
                    << convergeTime << ","
                    << tTime << endl;
		}//End Gamma
		
		delete [] linearR; delete [] linearA; delete [] linearM;
        //delete [] rGpu; delete [] aGpu; delete [] mGpu;
	}//End NpNd
	timer.stopTimer();
	totalTime = timer.getElapsedTime();

//	cout << "Function Name:  		" << functionName << endl;
//	cout << "Best Overall Fitness: 		" << setprecision(18) << bestFitnessOverall << endl;
//	cout << "Best Overall Probe #: 		" << setprecision(0) << bestProbeNumberOverall << endl;
//	cout << "Best Overall TimeStep:		" << bestTimeStepOverall << endl;
//	cout << "Best Gamma:		   	" << bestGamma << endl;
//	cout << "Best Probes Per Axis: 		" << bestNpNd << endl;
//	cout << "Best Number Of Probes:		" << bestNp << endl;
//	cout << "Overall Last Step:    		" << lastStepBestRun << endl;
	
	cout    << functionName << ","
            << bestFitnessOverall  << "," 
            << bestProbeNumberOverall << ","
            << bestTimeStepOverall << ","
            << bestGamma << ","
            << bestNpNd << ","
            << bestNp << ","
            << Nd << ","
            << lastStepBestRun << ","
            << bestNumEvals << ","
            << totalTime << endl;

	Nt = lastStepBestRun+1;
}
void CFO(int Np, int Nt, int Nd, long double Alpha, long double Beta, long double Frep, long double Gamma, int& lastStep, vector<long double> xMin, vector<long double> xMax, long double(*objFunc)(ldArray3D& ,int, int, int), string functionName) {

	long double DeltaXi, bestFitness = -INFINITY, deltaFrep = 0.1;
	long double DiagLength   = 0;
	long double positionTime = 0, correctionTime = 0, fitnessTime = 0, accelTime = 0,
                shrinkTime = 0,   convergeTime = 0,   totalTime = 0,   copyTime = 0;
	
	
	ldArray3D R(Np, ldArray2D(Nd, ldArray1D(Nt, 0)));
	ldArray3D A(Np, ldArray2D(Nd, ldArray1D(Nt, 0)));
	ldArray2D M(Np, ldArray1D(Nt, 0));
	ldArray1D bestFitnessArray(Nt);
	iArray1D bestProbeNumberArray(Nt);
	int bestTimeStep = 0, bestProbeNumber = 0;
	int numProbesPerAxis = Np / Nd, numEvals = 0;
	string fileName;

	CStopWatch timer, timer1;
	
	float *linearR = new float[Np*Nd],
	      *linearA = new float[Np*Nd], 
	      *linearM = new float[Np],
          *rGpu = new float[Np*Nd], 
          *aGpu = new float[Np*Nd], 
          *mGpu = new float[Np];
    
    timer1.startTimer();
	for (int i = 0; i < Nd; i++) {
		DiagLength += pow(xMax[i] - xMin[i], 2);
	}
	DiagLength = sqrt(DiagLength);

	//
	// Initial Probe Distribution
	//
	for (int i = 0; i < Nd; i++) {
		for (int p = 0; p < Np; p++) {
			R[p][i][0] = xMin[i] + Gamma * (xMax[i] - xMin[i]);
		}
	}
	for (int i = 0; i < Nd; i++) {
		DeltaXi = (xMax[i] - xMin[i]) / (numProbesPerAxis-1);
		int p;

		for (int k = 0; k < numProbesPerAxis; k++) {
			p = k + numProbesPerAxis * i;
			R[p][i][0] = xMin[i] + k * DeltaXi;
		}
	}

	//
	// Set Initial Acceleration to 0
	//
	for (int p = 0; p < Np; p++) {
		for (int i = 0; i < Nd; i++) {
			A[p][i][0] = 0;
		}
	}

	//
	// Compute Initial  Fitness
	//
	for (int p = 0; p < Np; p++) {
		M[p][0] = objFunc(R, Nd, p, 0);
		numEvals++;
	}

	//
	// Time Steps
	//
	lastStep = Nt;
	for (int j = 1; j < Nt; j++) {
		//
		// Compute new positions
		//
		timer.startTimer();
		for (int p = 0; p < Np; p++) {
			for (int i = 0; i < Nd; i++) {
				R[p][i][j] = R[p][i][j - 1] + A[p][i][j - 1];
			}
		}
		timer.stopTimer();
		positionTime += timer.getElapsedTime();
		//cout << "Position\n";

		//
		// Correct any errors
		//
		timer.startTimer();
		for (int p = 0; p < Np; p++) {
			for (int i = 0; i < Nd; i++) {
				if (R[p][i][j] < xMin[i]) {
					R[p][i][j] = max(xMin[i] + Frep * (R[p][i][j - 1] - xMin[i]), xMin[i]);
				}
				if (R[p][i][j] > xMax[i]) {
					R[p][i][j] = min(xMax[i] - Frep * (xMax[i] - R[p][i][j - 1]), xMax[i]);
				}
			}
		}
		timer.stopTimer();
		correctionTime += timer.getElapsedTime();
        //cout << "Correction\n";
		//
		// Update Fitness
		//
		timer.startTimer();
		for (int p = 0; p < Np; p++) {
			M[p][j] = objFunc(R, Nd, p, j);
			numEvals++;

			if(M[p][j] >= bestFitness){
				bestFitness 	= M[p][j];
				bestTimeStep 	= j;
				bestProbeNumber = p;
			}
		}
		timer.stopTimer();
		fitnessTime += timer.getElapsedTime();
		//cout << "Fitness\n";

        // Acceleration
		// Data for GPU Computations
		timer.startTimer();
		for(int p=0; p<Np; p++){
		    linearM[p] = M[p][j];
		    for(int i=0; i<Nd; i++){
		        linearR[p*Nd+i] = R[p][i][j];   
		        linearA[p*Nd+i] = A[p][i][j];   
		    }
		}
		timer.stopTimer();
		copyTime += timer.getElapsedTime();
        //cout << "Acceleration Copied \n";
        timer.startTimer();
        cudaMalloc((void**) &rGpu, Np*Nd*sizeof(float));  
        cudaMalloc((void**) &aGpu, Np*Nd*sizeof(float));
        cudaMalloc((void**) &mGpu, Np*sizeof(float));
    
        cudaMemcpy(rGpu, linearR, Np*Nd*sizeof(float), cudaMemcpyHostToDevice); 
        cudaMemcpy(aGpu, linearA, Np*Nd*sizeof(float), cudaMemcpyHostToDevice);
        cudaMemcpy(mGpu, linearM, Np*sizeof(float), cudaMemcpyHostToDevice);
		calculateA<<<Np, Nd>>>(rGpu, aGpu, mGpu, Np, Nd, Alpha, Beta);
        cudaMemcpy(linearA, aGpu, Np*Nd*sizeof(float), cudaMemcpyDeviceToHost);
        
        //cout << "Return from gPU\n";
	    for(int p=0; p<Np; p++){
	        for(int i=0; i<Nd; i++){
	            A[p][i][j] = linearA[p*Nd+i];   
	        }
		}
		//cout << "Copied Back\n";
		timer.stopTimer();
		accelTime += timer.getElapsedTime();
    	cudaFree(rGpu);
	    cudaFree(aGpu);
    	cudaFree(mGpu);		
		//cout << "Acceleration\n";

		//
		// Adjust Frep
		//
		Frep += deltaFrep;
		if(Frep > 1){
			 Frep = 0.05;
		}

		//
		// Shrink Boundaries
		//
		timer.startTimer();
		if(j % 10 == 0 && j >= 20){
			for(int i=0; i<Nd; i++){
				xMin[i] = xMin[i] + (R[bestProbeNumber][i][bestTimeStep] - xMin[i])/2.0;
				xMax[i] = xMax[i] - (xMax[i] - R[bestProbeNumber][i][bestTimeStep])/2.0;
			}
			//
			// Correct any errors
			//
			for (int p = 0; p < Np; p++) {
				for (int i = 0; i < Nd; i++) {

					if (R[p][i][j] < xMin[i]) {
						R[p][i][j] = max(xMin[i] + Frep * (R[p][i][j - 1] - xMin[i]), xMin[i]);
					}
					if (R[p][i][j] > xMax[i]) {
						R[p][i][j] = min(xMax[i] - Frep * (xMax[i] - R[p][i][j - 1]), xMax[i]);
					}
				}
			}
		}
		//cout << "shrink\n";
		timer.stopTimer();
		shrinkTime += timer.getElapsedTime();

		//
		// Test for fitness saturation
		//
		timer.startTimer();
		lastStep = j;
		if (hasFitnessSaturated(25, j, Np, Nd, M, R, DiagLength)) {
			timer.stopTimer();
			convergeTime += timer.getElapsedTime();
			break;
		}
		//cout << "Saturation\n";
		timer.stopTimer();
		convergeTime += timer.getElapsedTime();
	}//End Time Steps
    timer1.stopTimer();
    totalTime += timer1.getElapsedTime();
    
    
    //delete linearR; delete [] rGpu;
    //delete linearA; delete [] aGpu;
    //delete linearM; delete [] mGpu;
    R.clear(); A.clear(); M.clear();
    
//	cout << setw(23) << left << "Function Name:" << functionName << endl;
//	cout << setw(23) << left << "Best Fitness:" << setprecision(10) << bestFitness << endl;
//	cout << setw(23) << left << "Number of Probes:" << Np << endl;
//	cout << setw(23) << left << "Probes Per Axis:" << numProbesPerAxis << endl;
//	cout << setw(23) << left << "Best Probe #:" << setprecision(0) << bestProbeNumber << endl;
//	cout << setw(23) << left << "Best Gamma:" << Gamma << endl;		
//    cout << setw(23) << left << "Best Time Step:" << bestTimeStep << endl;
//	cout << setw(23) << left << "Last Step:" << setprecision(0) << lastStep << endl;
//	cout << setw(23) << left << "Evaluations:" << setprecision(0) << numEvals << endl;
//	cout << "-------------------------- Times --------------------------" 			 << endl;
//	cout << setw(23) << left << "Position:" 	<< setprecision(4) << positionTime 	 << endl;
//	cout << setw(23) << left << "Correction:" 	<< setprecision(4) << correctionTime << endl;
//	cout << setw(23) << left << "Fitness:" 		<< setprecision(4) << fitnessTime 	 << endl;
//    cout << setw(23) << left << "Copy:" 		<< setprecision(4) << copyTime 	 << endl;
//	cout << setw(23) << left << "Acceleration:" << setprecision(4) << accelTime 	 << endl;
//	cout << setw(23) << left << "Shrink:" 		<< setprecision(4) << shrinkTime 	 << endl;
//	cout << setw(23) << left << "Converge:" 	<< setprecision(4) << convergeTime 	 << endl;

	cout    << functionName << ","
            << bestFitness  << "," 
            << bestProbeNumber << ","
            << bestTimeStep << ","
            << Gamma << ","
            << numProbesPerAxis << ","
            << Np << ","
            << Nd << ","
            << lastStep << ","
            << numEvals << ","
            << positionTime << ","
            << correctionTime << ","
            << fitnessTime << ","
            << accelTime << ","
            << shrinkTime << ","
            << convergeTime << ","
            << totalTime << endl;
}

/*Function Name:  		F1 - CFO
Best Fitness:   		-0.000216501161992923608
Best Probe #:   		70
Best Time Step: 		55
Best Gamma:			    0.3
Probes Per Axis:		4
Number of Probes:		120
Last Step:      		55
*/

void run(long double oMin, long double oMax, long double (*rPtr)(ldArray3D&, int, int, int), string functionName){

    vector<long double> xMin;
	vector<long double> xMax;
	int lastStep;
	int Np, Nd, Nt;
	int NdMin = 50, NdMax = 100, NdStep = 10;
	long double Alpha, Beta, Frep, Gamma, min, max;
	
	Nd = 30; Nt = 1000; 
	min = oMin; max = oMax;
    MiniBenchmarkCFO(Np, Nd, Nt, Gamma, min, max, rPtr, functionName);
    cout << endl;
    for(int i=0; i<10;i++){
        Nt = 1000;
        xMin.clear(); xMin.resize(Nd, oMin);
        xMax.clear(); xMax.resize(Nd, oMax);
    	Alpha = 1; Beta = 2; Frep = 0.5;
    	CFO(Np, Nt, Nd,  Alpha, Beta, Frep, Gamma, lastStep, xMin, xMax, rPtr, functionName);
    }
    cout << endl;
    
    for(Nd=NdMin; Nd<=NdMax; Nd+=NdStep){ 
	    Nt = 1000;	min = oMin; max = oMax;
	    MiniBenchmarkCFO(Np, Nd, Nt, Gamma, min, max, rPtr, functionName);
	    cout << endl;
	    for(int i=0; i<10;i++){
	        Nt = 1000;
	        xMin.clear(); xMin.resize(Nd, min);
	        xMax.clear(); xMax.resize(Nd, max);
        	Alpha = 1; Beta = 2; Frep = 0.5;
        	CFO(Np, Nt, Nd,  Alpha, Beta, Frep, Gamma, lastStep, xMin, xMax, rPtr, functionName);
        }
        cout << endl;
	}
    cout << endl;
}
int main() {

	long double min, max;
	long double (*rPtr)(ldArray3D&, int, int, int) = NULL;
		
    cout  << "Function,Fitness,Best Probe,Best Timestep,Gamma,Probes per Axis,# Probes,Dimension,Last Step, NumEvals, Position Time,Correction Time,Fitness Time,Accel Time,Shrink Time,Converge Time,Total Time" << endl; 

    rPtr = &F1; min = -100; max = 100;
   	run(min, max, rPtr, "F1");
   	
   	rPtr = &F2; min = -10; max = 10;
   	run(min, max, rPtr, "F2");
   	
   	rPtr = &F3; min = -600; max = 600;
   	run(min, max, rPtr, "F3");

   	rPtr = &F4; min = -10; max = 10;
   	run(min, max, rPtr, "F4");  

	return 0;
}
