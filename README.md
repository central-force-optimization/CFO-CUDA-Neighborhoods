### Central Force Optimization in CUDA with Neighborhoods ###

A first attempt from a long time ago. This is a CFO algorithm implemented in CUDA with neighborhoods.

## Getting started ##
To get started, type "./build". 

## Related Publications ##

1. [Online](http://link.springer.com/article/10.1007%2Fs11227-011-0725-y) R. Green, L. Wang, M. Alam, and Richard Formato, "Central Force Optimization on a GPU: A Case Study in High Performance Metaheuristics,"" Journal of Supercomputing, vol. 62, no. 1, pp. 378-398, October 2012.

2. [Online](http://ieeexplore.ieee.org/xpl/freeabs_all.jsp?arnumber=5949667&abstractAccess=no&userType=inst) R. Green, L. Wang, M. Alam, R. Formato. "Central Force Optimization on a GPU: A Case Study in High Performance Metaheuristics using Multiple Topologies," IEEE Congress on Evolutionary Computation 2011 (CEC 2011), New Orleans, Louisiana, June 2011.